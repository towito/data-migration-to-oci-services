# Data migration to OCI – services


## Introduction

In general, there are two methods to be considered when it comes to migrating your data from its current location to cloud environment: online and offline. This is a simplified listing of Oracle’s capabilities in the area of data migration to Oracle Cloud Infrastructure (OCI). 

Data migration projects begin with a detailed inventory and assessment that span across entire legacy environment. This task usually covers:

- Applications: all software applications identity, their versions, and their dependencies
- Database: all database versions inventory and data types stored within them
- Regulatory Compliance: Applications and data types may be subject to regulatory compliance directives
- Storage: you may have different types of storage in legacy environments, so you have to know each storage type and the amount of data housed within each one
- Networking: source environment's network architecture to secure an optimal target network in cloud environment.


## Data migration to OCI – services

1. Migrate TB or PB of (NFS) data to object storage (without copying via the Internet) -> Oracle Cloud Infrastructure 
[Data Transfer Service](https://docs.oracle.com/en-us/iaas/Content/DataTransfer/Concepts/overview.htm#OverviewDataTransferService)
    - [Disk-based data transfer](https://docs.oracle.com/en-us/iaas/Content/DataTransfer/Concepts/disk_overview.htm) - data sent as files on encrypted customer-owned commodity disk to an Oracle transfer site
    - [Appliance-based data transfer](https://docs.oracle.com/en-us/iaas/Content/DataTransfer/Concepts/appliance_overview.htm) - data sent as files on secure, high-capacity, Oracle-supplied storage appliances to an Oracle transfer site.
2. A user-friendly, file interface to object storage for Windows (SMB) environments -> Open source/3rd-party FTP client apps, such as [Cyberduck](https://docs.oracle.com/en/programs/research/getting-started/moving-data-to-OCI/index.html#copying-data-to-oracle-cloud)
3. Bulk movement to/from local source directory to object storage (Windows or Linux) -> Oracle Cloud Infrastructure [Command Line Interface (CLI)](https://docs.oracle.com/en-us/iaas/Content/API/Concepts/cliconcepts.htm),OCI CLI running OCI Object Storage sync https://docs.oracle.com/en-us/iaas/tools/oci-cli/3.37.13/oci_cli_docs/cmdref/os.html
4. Bulk sync of files/directories to object storage (Windows, Linux or other cloud) -> [Rclone](https://docs.oracle.com/en/solutions/move-data-to-cloud-storage-using-rclone/configure-rclone-object-storage.html) open source command line program
5. Redirect on-premises backups or archives to object storage -> Oracle Partner 3rd-party backup apps, [RMAN](https://docs.oracle.com/en-us/iaas/dbcs/doc/back-database-object-storage-using-rman.html) (with Oracle Database Cloud Backup Module), Ressilio Connect (https://cloudmarketplace.oracle.com/marketplace/en_US/listing/72352805)
6. High-speed, dedicated, private connections to Oracle Cloud Infrastructure -> Oracle Cloud [FastConnect](https://docs.oracle.com/en-us/iaas/Content/Network/Concepts/fastconnectoverview.htm#FastConnect_Overview).

